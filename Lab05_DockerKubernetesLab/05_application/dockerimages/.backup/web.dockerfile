ARG VERSION=latest
FROM node:16

# Create app directory
WORKDIR /usr/src/app

RUN npm install
RUN npm start




LABEL app="cldinf-web"
EXPOSE 8080/tcp

ENTRYPOINT ["nginx", "-b"]

ENV MODULE_NAME=*
ENV GROUP_NAME=*
ENV API_HOST=*
ENV API_PORT=8080
