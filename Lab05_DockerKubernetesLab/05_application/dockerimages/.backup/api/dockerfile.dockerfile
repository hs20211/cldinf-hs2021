# syntax=docker/dockerfile:1
ARG VERSION=latest
FROM alpine
LABEL app="cldinf-api"

ENV PG_USER=*
ENV PG_PASSWORD=*
ENV PG_DATABASE=*
ENV PG_HOST=*

RUN go mod tidy
RUN go run main.go