conf t
feature pim
ip pim rp-address 192.168.0.100

int lo0
ip pim sparse-mode
exit
int lo1
ip pim sparse-mode
exit
int e1/1
ip pim sparse-mode
exit
int e1/2
ip pim sparse-mode
exit

int e1/3
switchport access vlan 140
exit
int e1/4
switchport access vlan 141
exit

vlan 140
    vn-segment 50140
    exit
vlan 141
    vn-segment 50141
    exit

feature nv overlay
feature vn-segment-vlan-based

interface nve1
    no shutdown
    source-interface lo1
    member vni 50140
        mcast-group 239.0.0.140
        exit
    member vni 50141
        mcast-group 239.0.0.141
        exit
exit

------------

router bgp 65000
    neighbor 10.0.0.21 remote-as 65000
        address-family ipv4 unicast
            exit
        exit
    neighbor 10.0.0.25 remote-as 65000
        address-family ipv4 unicast
            exit
        exit
    neighbor 10.0.0.29 remote-as 65000
        address-family ipv4 unicast
            exit
        exit
    neighbor 10.0.0.33 remote-as 65000
        address-family ipv4 unicast
            exit
        exit
    exit

neighbor 192.168.0.20 update-source lo0
----------------------------------------


interface vlan140
    no shutdown
    vrf member VRF-A
    no ip redirects
    ip address 172.21.140.20
    fabric forwarding mode anycast-gateway

interface vlan141
    no shutdown
    vrf member VRF-B
    no ip redirects
    ip address 172.21.141.10
    fabric forwarding mode anycast-gateway

vlan 999
  vn-segment 50999

interface vlan999
  no shutdown
  vrf member VRF-A
  ip forward


---------------------------------
feature bgp
router bgp 65000
  router-id 192.168.0.20
  neighbor 10.0.0.21 remote-as 65000
  address-family ipv4 unicast
  exit
  neighbor 10.0.128.9 remote-as 65000
  address-family ipv4 unicast
  exit
  neighbor 10.0.0.128.10 remote-as 65000
  address-family ipv4 unicast
exit
