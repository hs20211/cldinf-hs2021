Configure VLANs
--------
vlan 140
  vn-segment 50140

vlan 141
  vn-segment 50141

vlan 999
  vn-segment 50999


3. vrf on leaf
--------------

nv overlay evpn

router bgp 65000
    vrf context Tenant-1
        vni 65000
        rd auto
        address-family ipv4 unicast
            route-target both auto
            route-target both auto evpn



4. Configure the SVIs
---------------------

feature interface-vlan
fabric forwarding anycast-gateway-mac 0001.0001.0001

interface vlan140
    no shutdown
    vrf member Tenant-1
    no ip redirects
    ip address 172.21.140.1/24
    fabric forwarding mode anycast-gateway
exit

interface vlan141
    no shutdown
    vrf member Tenant-1
    no ip redirects
    ip address 172.21.141.1/24
    fabric forwarding mode anycast-gateway
exit

interface vlan999
    no shutdown
    vrf member Tenant-1
    no ip redirects
    fabric forwarding mode anycast-gateway
exit


5. configure /adjust nve
------------------------

interface nve1
    host-reachability protocol bgp
    member vni 50140
        mcast-group 239.0.0.140
        exit
    member vni 50141
        mcast-group 239.0.0.141
        exit
    member vni 50999
        mcast-group 239.0.0.99
        exit
    exit


6. configure ebgp evpn
-----------------------

router bgp 50000
    address-family l2vpn evpn
    retain route-target all
    send-community both
