conf t
feature pim
ip pim rp-address 192.168.0.100

int lo0
ip pim sparse-mode
exit
int lo1
ip pim sparse-mode
exit
int e1/1
ip pim sparse-mode
exit
int e1/2
ip pim sparse-mode
exit

int e1/3
switchport
switchport access vlan 140
exit
int e1/4
switchport
switchport access vlan 141
exit

vlan 140
    vn-segment 50140
    exit
vlan 141
    vn-segment 50141
    exit

feature nv overlay
feature vn-segment-vlan-based

interface nve1
    no shutdown
    source-interface lo1
    member vni 50140
        mcast-group 239.0.0.140
        exit
    member vni 50141
        mcast-group 239.0.0.141
        exit
exit
